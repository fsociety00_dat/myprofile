package com.example.myprofile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
    }

    private fun init() {
        Glide.with(this)
            .load("https://i.pinimg.com/564x/8b/95/f8/8b95f82995e881ada8d57f1a56672f8e.jpg")
            .placeholder(R.mipmap.ic_launcher).into(profile)

        Glide.with(this)
            .load("https://otakuarena.com/wp-content/uploads/2020/07/AAAABTdXX0pleEG-sDruJpbuhQ0YR9p9V5M8MR6M8ft9goUfVwNR3kTIW9VuVtCU6_hJlicomJcbHpdHinGbuHiLWzLPNGA4.jpg")
            .placeholder(R.mipmap.ic_launcher).into(cover)

        Glide.with(this)
            .load("https://sites.google.com/site/codegeasstypemoon2000/_/rsrc/1477577016189/home/characters/subcategories/britannian-imperial-family/lelouch-vi-britannia/Lelouch-vi-Britannia.JPG")
            .placeholder(R.mipmap.ic_launcher).into(image1)

        Glide.with(this)
            .load("https://static.wikia.nocookie.net/codegeass/images/b/b0/Code-geass-rs-season-2-image-015-chess.jpg/revision/latest?cb=20120105095644")
            .placeholder(R.mipmap.ic_launcher).into(image2)

        Glide.with(this)
            .load("https://static.zerochan.net/CODE.GEASS%3A.Hangyaku.no.Lelouch.full.2100639.jpg")
            .placeholder(R.mipmap.ic_launcher).into(image3)


        Glide.with(this)
            .load("https://fantasticmemes.files.wordpress.com/2014/04/code_geass_kururugi_suzaku_lamperouge_lelouch_wallpaper-41855.jpg")
            .placeholder(R.mipmap.ic_launcher).into(image4)


        Glide.with(this)
            .load("https://images4.fanpop.com/image/photos/17900000/Lelouch-and-Kallen-code-geass-17902701-1280-720.jpg")
            .placeholder(R.mipmap.ic_launcher).into(image5)






    }
}